package asgn2Restaurant;


import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.time.LocalTime;
import java.util.ArrayList;

import asgn2Customers.Customer;
import asgn2Customers.CustomerFactory;
import asgn2Exceptions.CustomerException;
import asgn2Exceptions.LogHandlerException;
import asgn2Exceptions.PizzaException;
import asgn2Pizzas.Pizza;
import asgn2Pizzas.PizzaFactory;

/**
 *
 * A class that contains methods that use the information in the log file to return Pizza 
 * and Customer object - either as an individual Pizza/Customer object or as an
 * ArrayList of Pizza/Customer objects.
 * 
 * @author Person A and Person B
 *
 */
public class LogHandler {

	/**
	 * Returns an ArrayList of Customer objects from the information contained in the log file ordered as they appear in the log file.
	 * @param filename The file name of the log file
	 * @return an ArrayList of Customer objects from the information contained in the log file ordered as they appear in the log file. 
	 * @throws CustomerException If the log file contains semantic errors leading that violate the customer constraints listed in Section 5.3 of the Assignment Specification or contain an invalid customer code (passed by another class).
	 * @throws LogHandlerException If there was a problem with the log file not related to the semantic errors above
	 * 
	 */
	public static ArrayList<Customer> populateCustomerDataset(String filename) throws CustomerException, LogHandlerException{
		ArrayList<Customer> customerList = new ArrayList<Customer>();
		try {
			BufferedReader br = new BufferedReader(new FileReader(filename));
			for (String line = br.readLine(); line != null; line = br.readLine()) {
				customerList.add(createCustomer(line));
			}
			br.close();
		} catch (IOException e) {
			throw new LogHandlerException("Invalid filename: " + e);
		} catch (CustomerException e) {
			throw new CustomerException(e);
		} catch (LogHandlerException e) {
			throw new LogHandlerException(e);
		}
			return customerList;
	}

	/**
	 * Returns an ArrayList of Pizza objects from the information contained in the log file ordered as they appear in the log file. .
	 * @param filename The file name of the log file
	 * @return an ArrayList of Pizza objects from the information contained in the log file ordered as they appear in the log file. .
	 * @throws PizzaException If the log file contains semantic errors leading that violate the pizza constraints listed in Section 5.3 of the Assignment Specification or contain an invalid pizza code (passed by another class).
	 * @throws LogHandlerException If there was a problem with the log file not related to the semantic errors above
	 * 
	 */
	public static ArrayList<Pizza> populatePizzaDataset(String filename) throws PizzaException, LogHandlerException{
		ArrayList<Pizza> pizzaList = new ArrayList<Pizza>();
		try {
			BufferedReader br = new BufferedReader(new FileReader(filename));
			for (String line = br.readLine(); line != null; line = br.readLine()) {
				pizzaList.add(createPizza(line));
			}
			br.close();
		} catch (IOException e) {
			throw new LogHandlerException("Invalid filename: " + e);
		} catch (PizzaException e) {
			throw new PizzaException(e);
		} catch (LogHandlerException e) {
			throw new LogHandlerException(e);
		} catch (Exception e) { 
			throw new LogHandlerException(e);
		}
			return pizzaList;
	}
	
	/**
	 * Creates a Customer object by parsing the  information contained in a single line of the log file. The format of 
	 * each line is outlined in Section 5.3 of the Assignment Specification.  
	 * @param line - A line from the log file
	 * @return- A Customer object containing the information from the line in the log file
	 * @throws CustomerException - If the log file contains semantic errors leading that violate the customer constraints listed in Section 5.3 of the Assignment Specification or contain an invalid customer code (passed by another class).
	 * @throws LogHandlerException - If there was a problem parsing the line from the log file.
	 */
	public static Customer createCustomer(String line) throws CustomerException, LogHandlerException{
		String[] customerArr;
		try {
			customerArr = line.split(",");
		} catch (Exception e) {
			throw new LogHandlerException("Error in log file line format: " + e);
		}
	    try {
	    	String name = customerArr[2];
	    	String mobileNumber = customerArr[3];
			String type = customerArr[4];
			int locationX = Integer.parseInt(customerArr[5]);
			int locationY = Integer.parseInt(customerArr[6]);
			return CustomerFactory.getCustomer(type, name, mobileNumber, locationX, locationY);
	    } catch (CustomerException e) {
	    	throw new CustomerException("Error constructing Customer object from line data: " + e);
	    } catch (Exception e) {
	    	throw new CustomerException("Error parsing data from log file line: " + e);
	    }
	}
	
	/**
	 * Creates a Pizza object by parsing the information contained in a single line of the log file. The format of 
	 * each line is outlined in Section 5.3 of the Assignment Specification.  
	 * @param line - A line from the log file
	 * @return- A Pizza object containing the information from the line in the log file
	 * @throws PizzaException If the log file contains semantic errors leading that violate the pizza constraints listed in Section 5.3 of the Assignment Specification or contain an invalid pizza code (passed by another class).
	 * @throws LogHandlerException - If there was a problem parsing the line from the log file.
	 */
	public static Pizza createPizza(String line) throws PizzaException, LogHandlerException{
		String[] pizzaArr;
		try {
			pizzaArr = line.split(",");
		} catch (Exception e) {
			throw new LogHandlerException("Error in log file line format: " + e);
		}
	    try {
	    	int quantity = Integer.parseInt(pizzaArr[8]);
	    	String[] orderTimeArray = pizzaArr[0].split(":");
			LocalTime orderTime = LocalTime.of(Integer.parseInt(orderTimeArray[0]),Integer.parseInt(orderTimeArray[1]), Integer.parseInt(orderTimeArray[2]));
			String[] deliveryTimeArray = pizzaArr[1].split(":");
			LocalTime deliveryTime = LocalTime.of(Integer.parseInt(deliveryTimeArray[0]),Integer.parseInt(deliveryTimeArray[1]), Integer.parseInt(deliveryTimeArray[2]));				
		    String pizzaCode = pizzaArr[7];
			return PizzaFactory.getPizza(pizzaCode, quantity, orderTime, deliveryTime);
	    } catch (PizzaException e) {
	    	throw new PizzaException("Error constructing Pizza object from line data: " + e);
	    } catch (Exception e) {
	    	throw new PizzaException("Error parsing data from log file line: " + e);
	    }
	    		
	}

}
