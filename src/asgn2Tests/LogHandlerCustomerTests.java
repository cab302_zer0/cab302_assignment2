package asgn2Tests;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Test;

import asgn2Customers.Customer;
import asgn2Customers.DriverDeliveryCustomer;
import asgn2Exceptions.CustomerException;
import asgn2Exceptions.LogHandlerException;
import asgn2Restaurant.LogHandler;

/**
 * A class that tests the methods relating to the creation of Customer objects in the asgn2Restaurant.LogHander class.
 *
 * @author Person A
 */
public class LogHandlerCustomerTests {
    //createCustomer
    @Test
    public void createCustomerTest() throws CustomerException, LogHandlerException{
        String line = "19:00:00,19:20:00,Casey Jones,0123456789,DVC,5,5,PZV,2";
        Customer runAnswer = LogHandler.createCustomer(line);
        Customer expectedAnswer = new DriverDeliveryCustomer("Casey Jones","0123456789",5,5);
        assertEquals(expectedAnswer,runAnswer);
    }
    //createCustomerException(customer)
    @Test(expected = CustomerException.class)
    public void createCustomerExceptionTest() throws CustomerException, LogHandlerException{
        String line = "19:00:00,19:20:00,Casey Jones,123456789,DVC,5,5,PZV,2";
        Customer runAnswer = LogHandler.createCustomer(line);
    }
    //createCustomerException(loghandler)
    @Test(expected = LogHandlerException.class)
    public void createCustomerLogExcpetion() throws CustomerException, LogHandlerException{
        String line = null;
        Customer runAnswer = LogHandler.createCustomer(line);
    }
    
    //populateCustomerData
    @Test
    public void populateCustomerTest() throws CustomerException, LogHandlerException{
        String filename = "logs/20170101.txt";
        ArrayList<Customer> test = LogHandler.populateCustomerDataset(filename);
        String runAnswer = test.get(0).getName();
        String expectedAnswer = "Casey Jones";
        assertEquals(expectedAnswer,runAnswer);
    }
    
    //populateCustomerDataException(customer)
    @Test(expected = CustomerException.class)
    public void populateCustomerException() throws CustomerException, LogHandlerException{
        String filename = "logs/20170101_incorrect_customer.txt";
        ArrayList<Customer> test = LogHandler.populateCustomerDataset(filename);
    }
    //populateCustomerDataException(loghandler)
    @Test(expected = LogHandlerException.class)
    public void populateCustomerLogException() throws CustomerException, LogHandlerException{
        String filename  = "";
        ArrayList<Customer> customer = LogHandler.populateCustomerDataset(filename);
    }
}
