package asgn2Tests;

import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.Assert;
import org.junit.Rule;
import java.time.LocalTime;
import asgn2Exceptions.PizzaException;
import asgn2Pizzas.MargheritaPizza;
import asgn2Pizzas.VegetarianPizza;
import asgn2Pizzas.MeatLoversPizza;
import asgn2Pizzas.PizzaTopping;

/**
 * A class that that tests the asgn2Pizzas.MargheritaPizza, asgn2Pizzas.VegetarianPizza, asgn2Pizzas.MeatLoversPizza classes. 
 * Note that an instance of asgn2Pizzas.MeatLoversPizza should be used to test the functionality of the 
 * asgn2Pizzas.Pizza abstract class. 
 * 
 * @author Person B
 *
 */
public class PizzaTests {
	@Rule
	public final ExpectedException exception = ExpectedException.none();
	
	@Test
	public void orderTooEarly() throws PizzaException {
		exception.expect(PizzaException.class);
		MargheritaPizza testPizza = new MargheritaPizza(3, LocalTime.of(15,00), LocalTime.of(20,30));
	}
	
	@Test
	public void orderTooLate() throws PizzaException {
		exception.expect(PizzaException.class);
		VegetarianPizza testPizza = new VegetarianPizza(3, LocalTime.of(23,30), LocalTime.of(23,45));
	}
	
	@Test
	public void deliveryTooLate() throws PizzaException {
		exception.expect(PizzaException.class);
		MargheritaPizza testPizza = new MargheritaPizza(3, LocalTime.of(20,00), LocalTime.of(22,30));
	}
	
	@Test
	public void deliveryPriorToOrder() throws PizzaException {
		exception.expect(PizzaException.class);
		VegetarianPizza testPizza = new VegetarianPizza(3, LocalTime.of(20,00), LocalTime.of(19,00));
	}
	
	@Test
	public void calculateCostPerPizza() throws PizzaException {
		MeatLoversPizza testPizza = new MeatLoversPizza(3, LocalTime.of(20,00), LocalTime.of(20,30));
		Assert.assertEquals(testPizza.getCostPerPizza(), 5.0, 0.01); // delta precision allowance of 0.01
	}
	
	@Test
	public void calculatePricePerPizza() throws PizzaException {
		MeatLoversPizza testPizza = new MeatLoversPizza(3, LocalTime.of(20,00), LocalTime.of(20,30));
		Assert.assertEquals(testPizza.getPricePerPizza(), 12.0, 0.01); // delta precision allowance of 0.01
	}
	
	@Test
	public void getOrderCost() throws PizzaException {
		MeatLoversPizza testPizza = new MeatLoversPizza(3, LocalTime.of(20,00), LocalTime.of(20,30));
		Assert.assertEquals(testPizza.getOrderCost(), 15.0, 0.01); // delta precision allowance of 0.01
	}
	
	@Test
	public void getOrderPrice() throws PizzaException {
		MeatLoversPizza testPizza = new MeatLoversPizza(3, LocalTime.of(20,00), LocalTime.of(20,30));
		Assert.assertEquals(testPizza.getOrderPrice(), 36.0, 0.01); // delta precision allowance of 0.01
	}
	
	@Test
	public void getOrderProfit() throws PizzaException {
		MeatLoversPizza testPizza = new MeatLoversPizza(3, LocalTime.of(20,00), LocalTime.of(20,30));
		Assert.assertEquals(testPizza.getOrderProfit(), 21.0, 0.01); // delta precision allowance of 0.01
	}
	
	@Test
	public void testHasTopping_True() throws PizzaException {
		VegetarianPizza testPizza = new VegetarianPizza(3, LocalTime.of(20,00), LocalTime.of(20,30));
		Assert.assertTrue(testPizza.containsTopping(PizzaTopping.EGGPLANT)); // delta precision allowance of 0.01
	}
	
	@Test
	public void testHasTopping_False() throws PizzaException {
		VegetarianPizza testPizza = new VegetarianPizza(3, LocalTime.of(20,00), LocalTime.of(20,30));
		Assert.assertFalse(testPizza.containsTopping(PizzaTopping.BACON)); // delta precision allowance of 0.01
	}
}
