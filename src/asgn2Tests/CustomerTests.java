package asgn2Tests;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import asgn2Customers.Customer;
import asgn2Customers.DroneDeliveryCustomer;
import asgn2Exceptions.CustomerException;

/**
 * A class that tests the that tests the asgn2Customers.PickUpCustomer, asgn2Customers.DriverDeliveryCustomer,
 * asgn2Customers.DroneDeliveryCustomer classes. Note that an instance of asgn2Customers.DriverDeliveryCustomer 
 * should be used to test the functionality of the  asgn2Customers.Customer abstract class. 
 * 
 * @author Person A
 * 
 *
 */
public class CustomerTests {
    //Test it throws Exception
    @Test(expected = CustomerException.class)
    public void testException()throws CustomerException{
        Customer runAnswer = new DroneDeliveryCustomer("", "", 5, 5);
    }
    
    @Test
    public void testGetType() throws CustomerException{
    	Customer testCustomer = new DroneDeliveryCustomer("Test", "0123456789",5,5);
        String runAnswer = testCustomer.getCustomerType();
        String expectedAnswer = "Drone Delivery";
        assertEquals(runAnswer,expectedAnswer);
    }
    
    @Test
    public void testGetDistance() throws CustomerException{
    	Customer testCustomer = new DroneDeliveryCustomer("Test", "0123456789",5,5);
        double runAnswer = testCustomer.getDeliveryDistance();
        double expectedAnswer = 7.071;
        assertEquals(runAnswer,expectedAnswer,0.2);
    }
    
    @Test
    public void testGetX() throws CustomerException{
    	Customer testCustomer = new DroneDeliveryCustomer("Test", "0123456789",5,5);
        int runAnswer = testCustomer.getLocationX();
        int expectedAnswer = 5;
        assertEquals(runAnswer,expectedAnswer);
    }
    
    @Test
    public void testGetY() throws CustomerException{
    	Customer testCustomer = new DroneDeliveryCustomer("Test", "0123456789",5,5);
        int runAnswer = testCustomer.getLocationY();
        int expectedAnswer = 5;
        assertEquals(runAnswer,expectedAnswer);
    }
    
    @Test
    public void testGetMobile() throws CustomerException{
    	Customer testCustomer = new DroneDeliveryCustomer("Test", "0123456789",5,5);
        String runAnswer = testCustomer.getMobileNumber();
        String expectedAnswer = "0123456789";
        assertEquals(runAnswer,expectedAnswer);
    }
    
    @Test
    public void testGetName() throws CustomerException{
    	Customer testCustomer = new DroneDeliveryCustomer("Test", "0123456789",5,5);
        String runAnswer = testCustomer.getName();
        String expectedAnswer = "Test";
        assertEquals(runAnswer,expectedAnswer);
    }
    
}
