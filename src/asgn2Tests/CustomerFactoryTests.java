package asgn2Tests;

import static org.junit.Assert.*;

import org.junit.Test;

import asgn2Customers.Customer;
import asgn2Customers.CustomerFactory;
import asgn2Customers.DriverDeliveryCustomer;
import asgn2Exceptions.CustomerException;

/**
 * A class the that tests the asgn2Customers.CustomerFactory class.
 * 
 * @author Person A
 *
 */
public class CustomerFactoryTests {
    //Test it works
    @Test
    public void testCustomerFactory()throws CustomerException{
        Customer runAnswer = CustomerFactory.getCustomer("DVC", "Test", "0123456789", 5, 5);
        Customer expectedAnswer = new DriverDeliveryCustomer("Test", "0123456789", 5, 5);
        assertEquals(expectedAnswer, runAnswer);
    }
    //Test exception (customer code)
    @Test(expected = CustomerException.class)
    public void testException()throws CustomerException{
        CustomerFactory.getCustomer("111", "Test", "0123456789", 5, 5);
    }
}
