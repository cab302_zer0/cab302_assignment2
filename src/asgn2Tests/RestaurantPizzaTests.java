package asgn2Tests;

import java.time.LocalTime;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import asgn2Exceptions.CustomerException;
import asgn2Exceptions.LogHandlerException;
import asgn2Exceptions.PizzaException;
import asgn2Pizzas.MargheritaPizza;
import asgn2Pizzas.Pizza;
import asgn2Restaurant.PizzaRestaurant;

/**
 * A class that tests the methods relating to the handling of Pizza objects in the asgn2Restaurant.PizzaRestaurant class as well as
 * processLog and resetDetails.
 * 
 * @author Person B
 *
 */
public class RestaurantPizzaTests {
    PizzaRestaurant incorrectTest;
    PizzaRestaurant correctTest;
    @Before
    public void construct() throws CustomerException, PizzaException, LogHandlerException{
        correctTest = new PizzaRestaurant();
        correctTest.processLog("logs/20170101.txt");
    }
    
    @Rule
	public final ExpectedException exception = ExpectedException.none();
    
    @Test
	public void getPizzaIndex() throws PizzaException {
		Pizza testPizza = correctTest.getPizzaByIndex(1);
		Assert.assertTrue(testPizza.equals(new MargheritaPizza(1, LocalTime.of(19,00), LocalTime.of(19,20))));
	}
    
    // Test invalid index
    @Test
	public void invalidPizzaIndex() throws PizzaException, CustomerException, LogHandlerException {
		exception.expect(PizzaException.class);
		incorrectTest = new PizzaRestaurant();
        incorrectTest.processLog("logs/20170101_incorrect_pizza.txt");
		Pizza testPizza = incorrectTest.getPizzaByIndex(3);
	}
    
    // Test index with invalid pizza
    @Test
    public void invalidPizzaByIndex() throws PizzaException {
    	exception.expect(PizzaException.class);
    	Pizza testPizza = correctTest.getPizzaByIndex(4);
    }
    
    // Test getting num of pizza orders on correct log
    @Test
    public void validGetPizzaOrders() throws PizzaException {
    	Assert.assertEquals(correctTest.getNumPizzaOrders(), 3); // delta precision allowance of 0.01);
    }
    
    // Test getting num of pizza orders on incorrect log
    @Test
    public void invalidGetPizzaOrders() throws PizzaException, CustomerException, LogHandlerException {
        exception.expect(PizzaException.class);
        incorrectTest = new PizzaRestaurant();
        incorrectTest.processLog("logs/20170101_incorrect_pizza.txt");
    	int impossible = correctTest.getNumPizzaOrders();
    }
    
    // Test getting total profit on correct log
    @Test
    public void validGetProfit() throws PizzaException {
    	Assert.assertEquals(correctTest.getTotalProfit(), 36.5, 0.01); // delta precision allowance of 0.01);
    }
    
    // Test getting total profit on incorrect log
    @Test
    public void invalidGetProfit() throws PizzaException, CustomerException, LogHandlerException {
    	exception.expect(PizzaException.class);
    	incorrectTest = new PizzaRestaurant();
        incorrectTest.processLog("logs/20170101_incorrect_pizza.txt");
    	double impossible = correctTest.getTotalProfit();
    }
    
}
