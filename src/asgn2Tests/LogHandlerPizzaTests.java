package asgn2Tests;

import java.time.LocalTime;
import java.util.ArrayList;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.Assert;
import asgn2Exceptions.LogHandlerException;
import asgn2Exceptions.PizzaException;
import asgn2Pizzas.Pizza;
import asgn2Pizzas.VegetarianPizza;
import asgn2Restaurant.LogHandler;

/** A class that tests the methods relating to the creation of Pizza objects in the asgn2Restaurant.LogHander class.
* 
* @author Person B
* 
*/
public class LogHandlerPizzaTests {
	@Rule
	public final ExpectedException exception = ExpectedException.none();
	
	// createPizza
	
	@Test
	public void createPizza() throws PizzaException, LogHandlerException {
		String line = "19:00:00,19:20:00,Casey Jones,0123456789,DVC,5,5,PZV,2";
		Pizza runAnswer = LogHandler.createPizza(line);
		Pizza expectedAnswer = new VegetarianPizza(2, LocalTime.of(19,00), LocalTime.of(19,20));
		Assert.assertEquals(runAnswer, expectedAnswer);
	}
	
	// This test creates an invalid pizza and should throw a PizzaException
	@Test
	public void invalidPizza() throws PizzaException, LogHandlerException {
		exception.expect(PizzaException.class);
		String line = "19:00:00,19:20:00,Casey Jones,0123456789,DVC,5,5,PZF,2";
		Pizza runAnswer = LogHandler.createPizza(line);
	}
	
	// This test tries to read an invalid log file line and should throw a LogHandlerException
	@Test
	public void invalidLogLine() throws PizzaException, LogHandlerException {
		exception.expect(LogHandlerException.class);
		String line = null;
		Pizza runAnswer = LogHandler.createPizza(line);
	}
	
	// populatePizzaDataset
	
	@Test
	public void testPizzaDatasetLoaded() throws PizzaException, LogHandlerException {
		String filename = "logs/20170101.txt";
		ArrayList<Pizza> test = LogHandler.populatePizzaDataset(filename);
		String runAnswer = test.get(1).getPizzaType();
		String expectedAnswer = "Margherita";
		Assert.assertEquals(runAnswer, expectedAnswer);
	}
	
	// This test creates an invalid pizza and should throw a PizzaException
	@Test
	public void invalidPizzaInDataset() throws PizzaException, LogHandlerException {
		exception.expect(PizzaException.class);
		String filename = "logs/20170101_incorrect_pizza.txt";
		ArrayList<Pizza> test = LogHandler.populatePizzaDataset(filename);
	}
		
	// This test tries to open an invalid filename and should throw a LogHandlerException
	@Test
	public void invalidFilename() throws PizzaException, LogHandlerException {
		exception.expect(LogHandlerException.class);
		String filename = null;
		ArrayList<Pizza> test = LogHandler.populatePizzaDataset(filename);
	}
}
