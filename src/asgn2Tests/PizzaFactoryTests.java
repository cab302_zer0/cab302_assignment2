package asgn2Tests;

import java.time.LocalTime;
import org.junit.Rule;
import org.junit.Test;
import org.junit.Assert;
import org.junit.rules.ExpectedException;
import asgn2Exceptions.PizzaException;
import asgn2Pizzas.PizzaFactory;
import asgn2Pizzas.Pizza;
import asgn2Pizzas.MargheritaPizza;
import asgn2Pizzas.VegetarianPizza;
import asgn2Pizzas.MeatLoversPizza;

/** 
 * A class that tests the asgn2Pizzas.PizzaFactory class.
 * 
 * @author Person B 
 * 
 */
public class PizzaFactoryTests {
	@Rule
	  public final ExpectedException exception = ExpectedException.none();
	
	@Test
	public void createMargherita() throws PizzaException {
		Pizza testPizza = PizzaFactory.getPizza("PZM", 3, LocalTime.of(20,00), LocalTime.of(20,30));
		Assert.assertTrue(testPizza.equals(new MargheritaPizza(3, LocalTime.of(20,00), LocalTime.of(20,30))));
	}
	
	@Test
	public void createVegetarian() throws PizzaException {
		Pizza testPizza = PizzaFactory.getPizza("PZV", 3, LocalTime.of(20,00), LocalTime.of(20,30));
		Assert.assertTrue(testPizza.equals(new VegetarianPizza(3, LocalTime.of(20,00), LocalTime.of(20,30))));
	}
	
	@Test
	public void createMeatLovers() throws PizzaException {
		Pizza testPizza = PizzaFactory.getPizza("PZL", 3, LocalTime.of(20,00), LocalTime.of(20,30));
		Assert.assertTrue(testPizza.equals(new MeatLoversPizza(3, LocalTime.of(20,00), LocalTime.of(20,30))));
	}
	
	@Test
	public void invalidPizza() throws PizzaException {
		exception.expect(PizzaException.class);
		Pizza testPizza = PizzaFactory.getPizza("PZH", 3, LocalTime.of(20,00), LocalTime.of(20,30));
	}
}
