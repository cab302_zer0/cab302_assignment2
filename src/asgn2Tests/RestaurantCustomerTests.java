package asgn2Tests;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

import asgn2Customers.Customer;
import asgn2Customers.DriverDeliveryCustomer;
import asgn2Customers.DroneDeliveryCustomer;
import asgn2Exceptions.CustomerException;
import asgn2Exceptions.LogHandlerException;
import asgn2Exceptions.PizzaException;
import asgn2Restaurant.PizzaRestaurant;

/**
 * A class that that tests the methods relating to the handling of Customer objects in the asgn2Restaurant.PizzaRestaurant
 * class as well as processLog and resetDetails.
 * 
 * @author Person A
 */
public class RestaurantCustomerTests {
	PizzaRestaurant incorrectTest;
    PizzaRestaurant correctTest;
    @Before
    public void construct() throws CustomerException, PizzaException, LogHandlerException{
        correctTest = new PizzaRestaurant();
        correctTest.processLog("logs/20170101.txt");
    }
	//get customer by index
    @Test
    public void testCustomerIndex() throws CustomerException{
        Customer runAnswer = correctTest.getCustomerByIndex(0);
        Customer expectedAnswer = new DriverDeliveryCustomer("Casey Jones", "0123456789",5,5);
        assertEquals(runAnswer,expectedAnswer);
    }
    //get customer by index exception
    @Test(expected = CustomerException.class)
    public void testCustomerIndexException() throws CustomerException, PizzaException, LogHandlerException{
    	incorrectTest = new PizzaRestaurant();
        incorrectTest.processLog("logs/20170101_incorrect_customer.txt");
        Customer runAnswer = incorrectTest.getCustomerByIndex(0);
    }
    //get num customer orders
    @Test
    public void testCustomerNumOrders() {
        int runAnswer = correctTest.getNumCustomerOrders();
        int expectedAnswer = 3;
        assertEquals(runAnswer,expectedAnswer);
    }
    //get num customer orders exception
    @Test(expected = CustomerException.class)
    public void testCustomerNumOrdersException() throws CustomerException, PizzaException, LogHandlerException{
    	incorrectTest = new PizzaRestaurant();
        incorrectTest.processLog("logs/20170101_incorrect_customer.txt");
        int runAnswer = incorrectTest.getNumCustomerOrders();
    }
    //get total delivery distance
    @Test
    public void testDeliveryDistance() {
        double runAnswer = correctTest.getTotalDeliveryDistance();
        double expectedAnswer = 15.0;
        assertEquals(runAnswer,expectedAnswer,0.2);
    }
}