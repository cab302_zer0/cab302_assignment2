package asgn2GUIs;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.table.DefaultTableModel;
import asgn2Customers.Customer;
import asgn2Pizzas.Pizza;
import asgn2Restaurant.PizzaRestaurant;
import java.awt.*;
import javax.swing.*;
import javax.swing.JFileChooser;


/**
 * This class is the graphical user interface for the rest of the system. 
 * Currently it is a �dummy� class which extends JFrame and implements Runnable and ActionLister. 
 * It should contain an instance of an asgn2Restaurant.PizzaRestaurant object which you can use to 
 * interact with the rest of the system. You may choose to implement this class as you like, including changing 
 * its class signature � as long as it  maintains its core responsibility of acting as a GUI for the rest of the system. 
 * You can also use this class and asgn2Wizards.PizzaWizard to test your system as a whole
 * 
 * 
 * @author Person A and Person B
 *
 */
public class PizzaGUI extends javax.swing.JFrame implements Runnable, ActionListener {
	
	private PizzaRestaurant restaurant;
	private String currentFile;
	private JLabel lblStatusPane;
	private JFileChooser selectFile;
	private DefaultTableModel pizzaData;
	private DefaultTableModel customerData;
	private JTable pizzaTable;
	private JTable customerTable;
	private JScrollPane pScrollPane;
	private JScrollPane cScrollPane;
	private final String[] pColumnNames = {"Index", "Pizza Type", "Quantity", "Order Price", "Order Cost", "Order Profit"};
	private final String[] cColumnNames = {"Index", "Name", "Mobile No.", "Delivery Type", "Coordinates", "Distance"};
	private final int NO_LOADED_ROWS = 1;
	// Default view is Pizza view. If false, the GUI is in Customer view.
	private boolean defaultView;
	private JButton btnNewButton;
	
	/**
	 * Creates a new Pizza GUI with the specified title 
	 * @param title - The title for the supertype JFrame
	 */
	public PizzaGUI(String title) {
		getContentPane().setLayout(null);
		this.currentFile = null;
		
		this.restaurant = new PizzaRestaurant();
		
		JButton btnLoadFile = new JButton("Load File...");
		btnLoadFile.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				selectFile();
			}
		});
		btnLoadFile.setBounds(383, 11, 100, 18);
		getContentPane().add(btnLoadFile);
		
		Object[][] pData = new Object[this.NO_LOADED_ROWS][this.pColumnNames.length];
		this.pizzaData = new DefaultTableModel(pData, this.pColumnNames);
		this.pizzaTable = new JTable();
		this.pizzaTable.setModel(pizzaData);
		this.pizzaTable.setBounds(10, 40, 578, 349);
		this.pScrollPane = new JScrollPane(this.pizzaTable);
		this.pScrollPane.setBounds(10, 40, 578, 320);
		getContentPane().add(this.pScrollPane);
		this.pizzaTable.setVisible(true);
		this.pScrollPane.setVisible(true);
		
		Object[][] cData = new Object[this.NO_LOADED_ROWS][this.cColumnNames.length];
		this.customerData = new DefaultTableModel(cData, this.cColumnNames);
		this.customerTable = new JTable();
		this.customerTable.setModel(customerData);
		this.customerTable.setBounds(10, 40, 578, 349);
		this.cScrollPane = new JScrollPane(this.customerTable);
		this.cScrollPane.setBounds(10, 40, 578, 320);
		getContentPane().add(this.cScrollPane);
		this.customerTable.setVisible(false);
		this.cScrollPane.setVisible(false);
		
		// Set default pizza view
		this.defaultView = true;
		
		JButton btnPizzaView = new JButton("Pizza View");
		btnPizzaView.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				toPizzaView();
			}
		});
		btnPizzaView.setBounds(102, 400, 200, 23);
		getContentPane().add(btnPizzaView);
		
		JButton btnCustomerView = new JButton("Customer View");
		btnCustomerView.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				toCustomerView();
			}
		});
		btnCustomerView.setBounds(312, 400, 200, 23);
		getContentPane().add(btnCustomerView);
		
		JLabel lblTitle = new JLabel("<html><h2>Pizza Restaurant Log Viewer</h2></html>");
		lblTitle.setBounds(10, 5, 284, 29);
		getContentPane().add(lblTitle);
		
		this.lblStatusPane = new JLabel("Please start by loading a file.");
		this.lblStatusPane.setBounds(10, 371, 575, 14);
		getContentPane().add(this.lblStatusPane);
		
		JButton btnNewButton = new JButton("Reset GUI");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				resetGUI();
			}
		});
		btnNewButton.setBounds(488, 11, 100, 18);
		getContentPane().add(btnNewButton);
		
		this.setTitle(title);
		run();
	}
	
	public void selectFile() {
		JFileChooser selectFile = new JFileChooser(".");
		//selectFile.setCurrentDirectory(new File(System.getProperty("user.home")));
		int fileResult = selectFile.showOpenDialog(null);
		if (fileResult == JFileChooser.APPROVE_OPTION) {
		    this.currentFile = selectFile.getSelectedFile().getAbsolutePath();
		    try {
		    	restaurant.processLog(this.currentFile);
		    } catch (Exception e) {
		    	this.currentFile = null;
		    	this.lblStatusPane.setText("Failed to load file due to exception: " + e);
		    	return;
		    }
		    
		    Object[][] pData;
		    if (restaurant.getNumPizzaOrders() > 0) {
		    	pData = new Object[restaurant.getNumPizzaOrders()][this.pColumnNames.length];
		    	for (int p = 0; p < restaurant.getNumPizzaOrders(); p++) {
		    		try {
		    			Pizza thisPizza = restaurant.getPizzaByIndex(p);
		    			pData[p][0] = p;
		    			pData[p][1] = thisPizza.getPizzaType();
			    		pData[p][2] = thisPizza.getQuantity();
			    		pData[p][3] = thisPizza.getOrderPrice();
			    		pData[p][4] = thisPizza.getOrderCost();
			    		pData[p][5] = thisPizza.getOrderProfit();
		    		} catch (Exception e) {
		    			for (int q = 0; q < this.pColumnNames.length; q++) {
				    		pData[0][q] = null;
				    	}
		    			this.lblStatusPane.setText("Failed to load list of pizza orders due to exception: " + e);
		    			return;
		    		}
		    	}
		    } else {
		    	pData = new Object[this.NO_LOADED_ROWS][this.pColumnNames.length];
		    	this.lblStatusPane.setText("No orders in this log file.");
		    }
		    
		    Object[][] cData;
		    if (restaurant.getNumCustomerOrders() > 0) {
		    	cData = new Object[restaurant.getNumCustomerOrders()][this.cColumnNames.length];
		    	for (int c = 0; c < restaurant.getNumCustomerOrders(); c++) {
		    		try {
		    			Customer thisCustomer = restaurant.getCustomerByIndex(c);
		    			cData[c][0] = c;
		    			cData[c][1] = thisCustomer.getName();
			    		cData[c][2] = thisCustomer.getMobileNumber();
			    		cData[c][3] = thisCustomer.getCustomerType();
			    		cData[c][4] = Integer.toString(thisCustomer.getLocationX())+", "+Integer.toString(thisCustomer.getLocationY());
			    		cData[c][5] = thisCustomer.getDeliveryDistance();
		    		} catch (Exception e) {
		    			for (int d = 0; d < this.cColumnNames.length; d++) {
				    		cData[0][d] = null;
				    	}
		    			this.lblStatusPane.setText("Failed to load list of customer orders due to exception: " + e);
		    			return;
		    		}
		    	}
		    } else {
		    	cData = new Object[this.NO_LOADED_ROWS][this.cColumnNames.length];
		    	this.lblStatusPane.setText("No orders in this log file.");
		    }
		    this.pizzaData = new DefaultTableModel(pData, this.pColumnNames);
		    this.pizzaTable.setModel(this.pizzaData);
		    this.customerData = new DefaultTableModel(cData, this.cColumnNames);
		    this.customerTable.setModel(this.customerData);
		    this.lblStatusPane.setText("File loaded successfully!");
		} else {
			this.currentFile = null;
		}
	}
	
	public void toPizzaView() {
		if (!(this.defaultView)) {
			this.customerTable.setVisible(false);
			this.cScrollPane.setVisible(false);
			this.pizzaTable.setVisible(true);
			this.pScrollPane.setVisible(true);
			this.defaultView = true;
			this.lblStatusPane.setText("Changed to pizza view.");
		}
	}
	
	public void toCustomerView() {
		if (this.defaultView) {
			this.pizzaTable.setVisible(false);
			this.pScrollPane.setVisible(false);
			this.customerTable.setVisible(true);
			this.cScrollPane.setVisible(true);
			this.defaultView = false;
			this.lblStatusPane.setText("Changed to customer view.");
		}
	}
	
	public void resetGUI() {
		this.restaurant.resetDetails();
		
		Object[][] pData = new Object[this.NO_LOADED_ROWS][this.pColumnNames.length];
		this.pizzaData = new DefaultTableModel(pData, this.pColumnNames);
		this.pizzaTable.setModel(this.pizzaData);
		
		Object[][] cData = new Object[this.NO_LOADED_ROWS][this.cColumnNames.length];
		this.customerData = new DefaultTableModel(cData, this.cColumnNames);
		this.customerTable.setModel(this.customerData);
		
		this.lblStatusPane.setText("GUI has been reset.");
	}
	
	@Override
	public void run() {
	    // load in center of screen
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		this.setLocation(dim.width/2-this.getSize().width/2, dim.height/2-this.getSize().height/2);
		
		this.setSize(611, 473);
		this.setVisible(true);
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		// TODO Auto-generated method stub
		
	}
}
