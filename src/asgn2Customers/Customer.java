package asgn2Customers;

import asgn2Exceptions.CustomerException;

/** An abstract class to represent a customer at the Pizza Palace restaurant.
 *  The Customer class is used as a base class of PickUpCustomer, 
 *  DriverDeliveryCustomer and DroneDeliverCustomer. Each of these subclasses overwrites
 *  the abstract method getDeliveryDistance. A description of the class's
 * fields and their constraints is provided in Section 5.2 of the Assignment Specification.  
 * 
 * @author Person B
*/
public abstract class Customer {


    /**
     *  This class represents a customer of the Pizza Palace restaurant.  A detailed description of the class's fields
     *  and parameters is provided in the Assignment Specification, in particular in Section 5.2. 
     *  A CustomerException is thrown if the any of the constraints listed in Section 5.2 of the Assignment Specification
     *  are violated. 
     *  
     * <P> PRE: True
     * <P> POST: All field values are set
     * 
     * @param name - The Customer's name 
     * @param mobileNumber - The customer mobile number
     * @param locationX - The customer x location relative to the Pizza Palace Restaurant measured in units of 'blocks' 
     * @param locationY - The customer y location relative to the Pizza Palace Restaurant measured in units of 'blocks' 
     * @param type - A human understandable description of this Customer type
     * @throws CustomerException if supplied parameters are invalid 
     * 
     */
    
    protected String name;
    protected String mobileNumber;
    protected int locationX;
    protected int locationY;
    protected String type;
    
    public Customer(String name, String mobileNumber, int locationX, int locationY, String type) throws CustomerException{
        
        // Check validity of name length and non-whitespace (via trim method)
        if (name.length() < 1 || name.length() > 20 || name.trim().length() < 1) {
            throw new CustomerException("Supplied Parameter Invalid: Name is either invalid length or only whitespaces.");
        }
        // Check validity of mobile length and format and all digits (via lambda function)
        if (mobileNumber.length() != 10 || !(mobileNumber.chars().allMatch(Character::isDigit))||!mobileNumber.substring(0,1).equals("0")) {
            throw new CustomerException("Supplied Parameter Invalid: Phone number has incorrect format.");
        }
        // Check validity of X delivery coordinate
        if (locationX < -10 || locationX > 10) {
            throw new CustomerException("Supplied Parameter Invalid: X coordinate outside delivery range.");
        }
        // Check validity of Y delivery coordinate
        if (locationY < -10 || locationY > 10) {
            throw new CustomerException("Supplied Parameter Invalid: Y coordinate outside delivery range.");
        }
        // Check validity of specified delivery type
        if (!(type.equals("Pick Up")|| type.equals("Driver Delivery")|| type.equals("Drone Delivery"))) {
            throw new CustomerException("Supplied Parameter Invalid: Delivery type not recognised.");
        }
        // Ensure customer is not at restaurant when requesting delivery
        if ((type.equals("Driver Delivery") || type.equals("Drone Delivery")) && locationX == 0 && locationY == 0) {
            throw new CustomerException("Delivery cannot be requested if set location is restaurant.");
        }
        // Ensure customer is at restaurant when requesting pickup
        if (type.equals("Pick Up") && !(locationX == 0 && locationY == 0)) {
            throw new CustomerException("Pick Up cannot be requested if set location is not restaurant.");
        }
        
        this.name = name;
        this.mobileNumber = mobileNumber;
        this.locationX = locationX;
        this.locationY = locationY;
        this.type = type;
    }
    
    /**
     * Returns the Customer's name.
     * @return The Customer's name.
     */
    public final String getName(){
        return this.name;
    }
    
    /**
     * Returns the Customer's mobile number.
     * @return The Customer's mobile number.
     */
    public final String getMobileNumber(){
        return this.mobileNumber;
    }

    /**
     * Returns a human understandable description of the Customer's type. 
     * The valid alternatives are listed in Section 5.2 of the Assignment Specification. 
     * @return A human understandable description of the Customer's type.
     */
    public final String getCustomerType(){
        return this.type;
    }
    
    /**
     * Returns the Customer's X location which is the number of blocks East or West 
     * that the Customer is located relative to the Pizza Palace restaurant. 
     * @return The Customer's X location
     */
    public final int getLocationX(){
        return this.locationX;
    }

    /**
     * Returns the Customer's Y location which is the number of blocks North or South 
     * that the Customer is located relative to the Pizza Palace restaurant. 
     * @return The Customer's Y location
     */
    public final int getLocationY(){
        return this.locationY;
    }

    /**
     * An abstract method that returns the distance between the Customer and 
     * the restaurant depending on the mode of delivery. 
     * @return The distance between the restaurant and the Customer depending on the mode of delivery.
     */
    public abstract double getDeliveryDistance();

    
    
    /**
     * Compares *this* Customer object with an instance of an *other* Customer object and returns true if  
     * if the two objects are equivalent, that is, if the values exposed by public methods are equal.
     *  You do not need to test this method.
     * 
     * @return true if *this* Customer object and the *other* Customer object have the same values returned for     
     * getName(),getMobileNumber(),getLocationX(),getLocationY(),getCustomerType().
     */
    @Override
    public boolean equals(Object other){
        Customer otherCustomer = (Customer) other;

        return ( (this.getName().equals(otherCustomer.getName()))  &&
            (this.getMobileNumber().equals(otherCustomer.getMobileNumber())) && 
            (this.getLocationX() == otherCustomer.getLocationX()) && 
            (this.getLocationY() == otherCustomer.getLocationY()) && 
            (this.getCustomerType().equals(otherCustomer.getCustomerType())) );         
    }

}
